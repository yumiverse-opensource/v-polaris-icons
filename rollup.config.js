import babel from '@rollup/plugin-babel'
import customTypes from './config/rollup/plugins/customTypes'
import vue from 'rollup-plugin-vue'
import commonjs from '@rollup/plugin-commonjs'
import pkg from './package.json'

const rollupConfig = {
  input: 'src/index.js',
  output: [
    { file: pkg.main, format: 'cjs' },
    { file: pkg.module, format: 'es' }
  ],
  external: ['vue'],
  onwarn: (warning, warn) => {
    if (warning.code === 'UNRESOLVED_IMPORT') {
      throw new Error(warning.message)
    }
    warn(warning)
  },
  plugins: [
    commonjs(),
    vue(),
    babel({
      exclude: 'node_modules/**',
      extensions: ['.js'],
      envName: 'production',
    }),
    customTypes({
      include: 'src/index.js',
    }),
  ],
}

export default rollupConfig
