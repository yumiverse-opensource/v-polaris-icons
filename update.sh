#!/bin/bash

sourcefile=src/index.js
> $sourcefile
for f in node_modules/\@shopify/polaris-icons/images/*;
do
  svgContent=`cat $f`
  svgContent=${svgContent/<svg/<svg :class=\"data.class\" v-bind=\"data.attrs\"}
  filename=$(basename $f)
  filenameNoExt=`echo $filename | sed -e 's/\.svg$//'`
  echo "<template functional>$svgContent</template>" > src/icons/${filenameNoExt}.vue
  upperCamelCase=`echo $filenameNoExt | sed -r 's/(^|_|-)([a-z])/\U\2/g'`
  echo export { default as ${upperCamelCase} } from \'./icons/${filenameNoExt}.vue\' >> $sourcefile
done
